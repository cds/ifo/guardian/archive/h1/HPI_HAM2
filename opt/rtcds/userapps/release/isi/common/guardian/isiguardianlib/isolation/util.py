import time

from ezca.ligofilter import LIGOFilterManager

from .. import const as top_const
from .. import util as top_util
from . import const as iso_const

##################################################
# NOTE: the following functions are BLOCKING and will not return until
# all changes have completed their ramps.  This could be multiple
# seconds if ramp=True.  This function should then be used sparingly
# (such as only in INIT).

def clear_iso_filters(ramp=False):
    lfm_iso = LIGOFilterManager(['ISO_'+dof for dof in top_const.DOF_LIST], ezca)
    if ramp:
        ramp_times = iso_const.ISOLATION_CONSTANTS['RAMP_DOWN_TIMES']
        gains = iso_const.ISOLATION_CONSTANTS['RAMP_DOWN_GAINS']
    else:
        ramp_times = [0]
        gains = [0]
    for i in range(len(ramp_times)):
        lfm_iso.ramp_gains(gain_value=gains[i],
                           ramp_time=ramp_times[i],
                           wait=True)
    lfm_iso.all_do('all_off', wait=True)


def deisolate():
    clear_iso_filters(ramp=True)

##################################################

def get_degs_of_freedom_string(degs_of_freedom):
    if len(degs_of_freedom) == 0:
        return ''
    if len(degs_of_freedom) == len(top_const.DOF_LIST):
        return 'ALL'
    return '_'.join(degs_of_freedom)


@top_util.check_arg(0, 'control_level', iso_const.ISOLATION_CONSTANTS['LEVEL_FILTERS'])
def check_if_isolation_loops_on(control_level):
    filter_channel_names = ['ISO_'+dof for dof in iso_const.ISOLATION_CONSTANTS['ALL_DOF']]
    filter_module_names =\
            iso_const.ISOLATION_CONSTANTS['LEVEL_FILTERS'][control_level]['MAIN']\
            + iso_const.ISOLATION_CONSTANTS['LEVEL_FILTERS'][control_level]['BOOST']
    error_message = top_util.check_if_requested_filter_modules_loaded(
                    filter_channel_names = filter_channel_names,
                    button_names = iso_const.ISOLATION_CONSTANTS['ONLY_ON_BUTTONS'],
                    filter_module_names = filter_module_names,)
    error_message += top_util.check_if_filters_have_correct_gain(
                    filter_channel_names,
                    iso_const.ISOLATION_CONSTANTS['RAMP_UP_GAINS'][-1])
    return error_message


def switch_gs13_gain(command, doflist):
    """Swtich GS13 analog gain between HIGH and LOW"""
    lf_gs13 = LIGOFilterManager(['GS13INF_'+dof for dof in doflist], ezca)
    if command == 'HIGH':
        log('Switching GS13s to high gain')
        lf_gs13.all_do('switch', 'FM4','OFF','FM5','OFF')
    elif command == 'LOW':
        log('Switching GS13s to low gain')
        lf_gs13.all_do('switch', 'FM4', 'ON', 'FM5', 'ON')
    # To account for the 2s zero-crossing timeout
    time.sleep(3)
    return


def turn_FF(command, doflist):
    """Turn FF path on/off"""
    for ff_type in doflist.keys():
        for cart in doflist[ff_type]:
            chan = ff_type + '_' + cart
            if command == 'ON':
                log(['Turning ' + ff_type + ' path ON'])
                ezca.switch(chan, 'OUTPUT', 'ON')
            if command == 'OFF':
                log(['Turning ' + ff_type + ' path OFF'])
                ezca.switch(chan, 'OUTPUT', 'OFF')
    return 

## deprecated blend filter code
#@top_util.check_arg(0, 'blend_number', range(1, MAX_BLEND_NUMBER))
#def check_if_blend_filters_are_on(blend_number):
#    if top_const.CHAMBER_TYPE == 'HPI':
#        from ..HPI.util import get_all_blend_filter_names
#        return top_util.check_if_filters_are_in_requested_state(get_all_blend_filter_names(),
#                ['INPUT', 'OUTPUT', 'DECIMATION'], ['FM'+str(blend_number)])
#    else:
#        return ErrorMessage()
