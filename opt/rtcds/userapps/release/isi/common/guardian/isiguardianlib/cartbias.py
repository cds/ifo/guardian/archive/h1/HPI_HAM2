import time
from collections import defaultdict

from ezca.ezcaPV import EzcaPV
from ezca.ramp import Ramp


_no_value = object()

BIAS_RAMP_TIME = 5.
RAMP_READ_TIME_STEP = 0.125
AVERAGING_SAMPLE_FREQUENCY = 4. # Hz
AVERAGING_TIME = 5.


class CartesianBiasManager(object):
    def __init__(self, device_name, ezca, deg_of_free_list):
        self.ezca = ezca
        self.deg_of_free_list = deg_of_free_list
        self.device_name = device_name

        for _deg_of_free in deg_of_free_list:
            for suffix in ['LOCATIONMON', 'TARGET']:
                setattr(self, _deg_of_free+'_'+suffix, self.__get_ezcapv(_deg_of_free, suffix))

            def is_ramping(value, _deg_of_free=_deg_of_free):
                return bool(value)

            def start_ramp(value, ramp_time, _deg_of_free=_deg_of_free):
                TRAMP = self.__get_ezcapv(_deg_of_free, 'TRAMP')
                SETPOINT_NOW = self.__get_ezcapv(_deg_of_free, 'SETPOINT_NOW')

                TRAMP.put(ramp_time, wait=True)

                if SETPOINT_NOW.get() != value:
                    SETPOINT_NOW.put(value, wait=True)
                    return True
                return False

            setattr(self, _deg_of_free + '_ramp',
                    Ramp(self.__get_pvname(_deg_of_free, 'RAMPSTATE'),
                        is_ramping,
                        start_ramp,
                        self.ezca),)

    def __repr__(self):
        return '%s(device_name=%r, ezca=%r, deg_of_free_list=%r)'\
                % (self.__class__.__name__, self.device_name, self.ezca, self.deg_of_free_list)

    def __get_pvname(self, deg_of_free, suffix):
        return '_'.join([self.device_name, deg_of_free, suffix])

    def __get_ezcapv(self, deg_of_free, suffix):
        return EzcaPV(self.__get_pvname(deg_of_free, suffix), ezca=self.ezca)

    def restore_all_offsets(self, ramp_time, wait=True):
        # start the bias ramping on each bias channel
        requested_value_dict = dict((deg_of_free, getattr(self, deg_of_free+'_TARGET').get())\
                for deg_of_free in self.deg_of_free_list)
        self.set_all_biases(ramp_time, requested_value_dict, wait)

    def set_all_biases(self, ramp_time, requested_value_dict=_no_value, wait=True):
        if requested_value_dict is _no_value:
            value = self.__get_average_value_dict()
        else:
            value = requested_value_dict

        for deg_of_free in self.deg_of_free_list:
            getattr(self, deg_of_free+'_ramp').start_ramp(value[deg_of_free], ramp_time)

        # wait for all the ramps to complete
        if wait:
            time.sleep(ramp_time)
            while self.is_ramping():
                time.sleep(RAMP_READ_TIME_STEP)
        
    def store_targets(self):
        for deg_of_free in self.deg_of_free_list:
            TARGET = getattr(self, deg_of_free+'_TARGET')
            SETPOINT_NOW = getattr(self, deg_of_free+'_SETPOINT_NOW')
            TARGET.put(SETPOINT_NOW.get(), wait=True)

    def is_ramping(self):
        for deg_of_free in self.deg_of_free_list:
            if getattr(self, deg_of_free + '_ramp').is_ramping():
                return True
        return False
            
    def __get_average_value_dict(self, seconds=AVERAGING_TIME, frequency=AVERAGING_SAMPLE_FREQUENCY):
        average_value = defaultdict(float)
        number_of_samples = int(seconds*frequency)
        for i in range(number_of_samples):
            for deg_of_free in self.deg_of_free_list:
                LOCATIONMON = getattr(self, deg_of_free+'_LOCATIONMON')
                average_value[deg_of_free] += LOCATIONMON.get()/float(number_of_samples)
            time.sleep(1./frequency)
        return average_value
